export class GoogleInputDto {
  q: string;
  source: string = 'en';
  target: string = 'it';
  readonly format: string = 'text';

  constructor(value: string, from: string, to: string) {
    this.q = value;
    this.source = from;
    this.target = to;
  }
}

export interface GoogleResponse {
  data: {
    translations: { translatedText: string }[];
  }
}
