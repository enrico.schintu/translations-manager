import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {GoogleTranslateService} from "./services/google-translate.service";

@Component({
  selector: 'google-translate',
  templateUrl: './google-translate.component.html',
  styleUrls: ['./google-translate.component.scss']
})
export class GoogleTranslateComponent implements OnInit {


  constructor(public googleTranslateService: GoogleTranslateService) {
  }

  ngOnInit() {

  }

  setGoogleTranslateApiKey(key: string): void {
    try {
      this.googleTranslateService.setGoogleKey(key);
    } catch (e) {
      console.warn('si è verificato un errore');
    }
  }

}
