import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {GoogleInputDto} from "../models/service.model";
import {environment} from "../../../../environments/environment";

const API_URL = 'https://translation.googleapis.com/language/translate/v2?key=';
const ERROR_MSG = 'You must provide a valid Google Api key. Check the following URL to get a valid API key:\n https://help.memsource.com/hc/en-us/articles/115003461051-Google-Translate-API-Key';
const LOCAL_STORAGE_KEY = 'GOOGLE-TRANSLATE-API-KEY';

@Injectable({
  providedIn: 'root'
})
export class GoogleTranslateService {

  public serviceEnabled: boolean = false;
  private googleApiKey: string = environment.googleApiKey;

  constructor(private _http: HttpClient) {
    this.initService();
  }

  private initService(): void {
    const storedApiKey = localStorage.getItem(LOCAL_STORAGE_KEY) || this.googleApiKey;
    storedApiKey
      ? this.activateService(storedApiKey)
      : console.info(`The google translate service is disabled.`);
  }

  translate(googleDto: GoogleInputDto, inputGoogleKey?: string): Observable<object> {
    const GOOGLE_API_KEY = inputGoogleKey ? inputGoogleKey : this.googleApiKey;
    if (!GOOGLE_API_KEY) {
      window.alert(ERROR_MSG);
      throw Error(ERROR_MSG);
    }
    return this._http.post(API_URL + GOOGLE_API_KEY, googleDto);
  }

  setGoogleKey(key: string): void {
    const testApiKey = new GoogleInputDto(key, 'it', 'en');
    this.translate(testApiKey, key).subscribe((result) => {
      this.activateService(key);
    }, (error) => {
      window.alert(ERROR_MSG);
      throw Error(error);
    });
  }

  private activateService(apiKey: string): void {
    this.googleApiKey = apiKey;
    this.serviceEnabled = true;
    localStorage.setItem(LOCAL_STORAGE_KEY, apiKey);
    console.info(`The google translate service is now enabled.`);
  }
}
