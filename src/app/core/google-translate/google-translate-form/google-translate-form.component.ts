import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {GoogleTranslateService} from "../services/google-translate.service";
import {GoogleInputDto, GoogleResponse} from "../models/service.model";

@Component({
  selector: 'google-translate-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './google-translate-form.component.html',
  styleUrls: ['./google-translate-form.component.scss']
})
export class GoogleTranslateFormComponent implements OnInit {

  public translatedValue: string = '';
  public sourceLanguage: string = 'it';
  public targetLanguage: string = 'en';

  constructor(private googleTranslateService: GoogleTranslateService,
              private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  public translateByGoogle(value: string, from: string, to: string) {
    const dto = new GoogleInputDto(value, from, to);
    this.googleTranslateService.translate(dto).subscribe((googleResponse: GoogleResponse) => {
      this.translatedValue = googleResponse.data.translations[0].translatedText;
      this.changeDetectorRef.markForCheck();
    });
  }

}
