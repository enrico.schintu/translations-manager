import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GoogleTranslateComponent} from "./google-translate.component";
import {GoogleTranslateService} from "./services/google-translate.service";
import { GoogleTranslateFormComponent } from './google-translate-form/google-translate-form.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    GoogleTranslateComponent,
    GoogleTranslateFormComponent
  ],
  exports: [
    GoogleTranslateComponent
  ],
  providers: [
    GoogleTranslateService
  ]
})
export class GoogleTranslateModule { }
