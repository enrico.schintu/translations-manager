export class DownloadFile {
	name: string;
	url: any;

	constructor(name: string, url: any) {
		this.name = name;
		this.url = url;
	}
}
