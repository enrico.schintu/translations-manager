import {Component, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DomSanitizer} from '@angular/platform-browser';

import {Message} from 'primeng/components/common/api';

import {TranslationsManagerService} from './translations-manager.service';
import {TranslationDto} from './translation';
import {DownloadFile} from './download-file';

@Component({
  selector: 'app-translations-manager',
  templateUrl: './translations-manager.component.html',
  styleUrls: ['./translations-manager.component.scss'],
})

export class TranslationsManagerComponent implements OnInit {

  public languages: Array<string> = [];

  /** Map<LanguageCode, TranslationDto[]> */
  public translationsMap: Map<string, Array<any>> = new Map();

  /** Map<translationCode, TranslationDto[]> */
  public rows: Map<string, Array<any>> = new Map();

  public translationsToUpdate: Array<TranslationDto>;
  public translationsToDelete: Array<TranslationDto>;
  public closeResult: string;

  public msgs: Message[] = [];
  public searchFilter: string;
  public source: string;
  public downloadJsonHref: any;
  public downloads: DownloadFile[] = new Array<DownloadFile>();
  public newTranslation: boolean = false;
  public jsonExample: Object = {
    'login.btn': 'x',
    'login.welcome': 'welcome to my Angular App',
    'home.new': 'New post!'
  };

  private JSON_FILE = 'file';
  private API_URL = 'api';
  public translatedValue: any;

  constructor(private translationManagerService: TranslationsManagerService,
              private modalService: NgbModal,
              private sanitizer: DomSanitizer) {
    // Sbiancando il searchFilter vengono visualizzati in pagina tutti i risultati
    this.searchFilter = '';
    this.source = this.JSON_FILE;
  }

  ngOnInit() {
    this.refreshTranslations();
  }

  /** Ricarica le traduzioni. Se la sorgente è API_URL allora contatta il Back-End altrimenti
   *  se la sorgente è JSON_FILE l'utente ritorna nella pagina di caricamento file
   *  */
  public refreshTranslations() {
    this.languages = new Array<string>();
    this.rows = new Map<string, Array<TranslationDto>>();
    this.translationsMap = new Map<string, Array<TranslationDto>>();
    switch (this.source) {
      case this.API_URL:
        this.loadTranslationsFromApi();
        break;
      case this.JSON_FILE:
        this.generateDownloadJsonUri();
        break;
    }
  }

  /** Apertura del modale per inserimento/aggiornamento/cancellazione traduzioni.
   *  Se viene passato il translation code il modale permette l'aggiornamento o la cancellazione di una traduzione esistente.
   *  Se non viene passato il translationCode il modale permette l'inserimento di una nuova traduzione.
   */
  public open(content: any, translationCode: string) {
    this.translationsToUpdate = new Array<TranslationDto>();
    if (!translationCode) {
      this.newTranslation = true;
    }
    this.languages.forEach(langCode => {
      let translation = translationCode ? JSON.parse(JSON.stringify(this.getTranslationValue(translationCode, langCode))) : new TranslationDto();
      translation.languageCode = langCode;
      translation.translationCode = translationCode;
      translation.user = 'TranslationsUtils';
      this.translationsToUpdate.push(translation);
    });

    this.openModal(content);
  }

  /** Apre ngbModal passando un ng-template come input*/
  public openModal(content: any) {
    this.modalService.open(content, {size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.newTranslation = false;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.newTranslation = false;
    });
  }

  /** Metodo che si appoggia al servizio translationManagerService per aggiornare le traduzioni selezionate */
  public updateSelectTranslations(): void {
    this.translationsToUpdate.forEach(translation => {
      if (!translation.value || translation.value === '') {
        translation.deleteFlag = true;
      }
    });
    switch (this.source) {
      case this.API_URL:
        console.log('Richiesta di aggiornamento traduzioni');
        this.translationManagerService.updateTranslations(this.translationsToUpdate).subscribe(
          (data) => {
            this.showSuccess(`Salvataggio completato con successo!`);
            this.refreshTranslations();
          },
          (err) => this.showError(`Errore durante l'aggiornamento delle traduzioni`)
        );
        break;
      case this.JSON_FILE:
        // Inserimento in mappa dei nuovi valori traduzione
        this.translationsToUpdate.forEach(translation => {
          let values = this.translationsMap.get(translation.languageCode);
          if (!values || values.length < 1) {
            values = new Array<TranslationDto>();
          }
          values.push(translation);
          this.translationsMap.set(translation.languageCode, values);
        });
        this.rows.set(this.translationsToUpdate[0].translationCode, this.translationsToUpdate);
        this.generateDownloadJsonUri();
        this.newTranslation = false;
        break;
    }

  }

  /** Usata in pagina per capire se sono presenti i dati delle traduzioni o meno */
  public noData(): boolean {
    return (this.translationsMap.size < 1 || this.languages.length < 1);
  }

  /** Metodo utilizzato per *ngFor in pagina, permette di ottenere la lista di translationCode filtrata per la chiave
   *  di ricerca searchFilter
   */
  public getKeys() {
    return Array.from(this.rows.keys())
      .filter(translationCode => translationCode.includes(this.searchFilter))
      .sort((code1, code2) => {
        if (code1 > code2) {
          return 1;
        } else if (code1 < code2) {
          return -1;
        }
        return 0;
      });
  }

  /** Restituisce la traduzione per un determinato translationCode e languageCode */
  public getTranslationValue(translationCode: string, languageCode: string): TranslationDto {
    let result = Array.from(this.rows.get(translationCode)).find(translation => {
      return (translation.translationCode === translationCode && translation.languageCode === languageCode);
    });
    return (result) ? result : new TranslationDto();
  }

  /** Get the fils provided in the upload panel and put the datas in the map properties */
  public loadTranslationsFromFile(translations: Map<string, any>): void {
    Array.from(translations.keys()).forEach(fileName => {
      this.languages.push(fileName);
      // La mappa translationsMap si valorizza con languageCode e TranslationDto
      // La mappa rows si valorizza con translationCode e TranslationDto
      const localTranslationMap = this.jsonToStrMap(translations.get(fileName));
      Array.from(localTranslationMap.keys()).forEach(key => {
        let translationDto: TranslationDto = new TranslationDto();
        translationDto.translationCode = key;
        translationDto.languageCode = fileName;
        translationDto.value = localTranslationMap.get(key);
        let keyValues = this.translationsMap.get(fileName);
        if (!keyValues) {
          keyValues = new Array<TranslationDto>();
        }
        keyValues.push(translationDto);
        this.translationsMap.set(fileName, keyValues);
        keyValues = this.rows.get(key);
        if (!keyValues) {
          keyValues = new Array<TranslationDto>();
        }
        keyValues.push(translationDto);
        this.rows.set(key, keyValues);
      });
    });
    this.generateDownloadJsonUri();
  }

  /** Contatta il server Back-End per ottenere le lingue aggiornate appoggiandosi a {@link this.translationManagerService} */
  private loadTranslationsFromApi() {
    // Accedere a tutte le lingue
    this.translationManagerService.retrieveAvailableLanguages().subscribe(codes => {
      // Recupera tutte le traduzioni utilizzando il languageCode delle lingue disponibili
      this.languages.push(...codes);
      codes.forEach(languageCode => {
        this.translationManagerService.retrieveLanguageTranslations(languageCode).subscribe(res => {
          let translations: Array<TranslationDto> = res;
          translations.forEach(translation => {
            let values: Array<TranslationDto> = this.rows.get(translation.translationCode);
            if (!values || values.length < 1) {
              values = new Array<TranslationDto>();
            }
            values.push(translation);
            this.rows.set(translation.translationCode, values);
          });
          // Valorizza la mappa usata per gestire le traduzioni in pagina
          this.translationsMap.set(languageCode, translations);
        });
      });
    });
  }

  /** Aggiorna gli url per i bottoni download */
  private generateDownloadJsonUri() {
    this.downloads = new Array<DownloadFile>();
    let translationsList: Array<TranslationDto> = new Array<TranslationDto>();
    let files: Map<string, Map<string, string>> = new Map<string, Map<string, string>>();

    // per ogni chiave ho una lista di translationDto
    Array.from(this.translationsMap.keys()).forEach(fileName => {
      translationsList = this.translationsMap.get(fileName);
      translationsList.forEach(translation => {
        let fileValues: Map<string, string> = files.get(fileName);
        if (!fileValues || fileValues.size < 1) {
          fileValues = new Map<string, string>();
        }
        if (!translation.deleteFlag) {
          // Non vengono considerate le traduzioni per le quali non è stato assegnato alcun valore
          fileValues.set(translation.translationCode, translation.value);
          files.set(fileName, fileValues);
        }
      });
    });

    let object = {};
    Array.from(files.keys()).forEach(file => {
      let fileTranslations: Map<string, string> = files.get(file);

      // Versione ad albero
      // fileTranslations.forEach((value, key) => {
      // 	let keys = key.split('.'),
      // 		last = keys.pop();
      // 	keys.reduce((r, a) => r[a] = r[a] || {}, object)[last] = value;
      // });

      // versione classica ordinata per chiave
      object = Array.from(fileTranslations.entries()).sort((code1, code2) => {
        if (code1 > code2) {
          return 1;
        } else if (code1 < code2) {
          return -1;
        }
        return 0;
      }).reduce((main, [key, value]) => ({...main, [key]: value}), {});
      this.downloads.push(new DownloadFile(file, this.sanitizer.bypassSecurityTrustUrl('data:text/json;charset=UTF-8,' + encodeURIComponent(JSON.stringify(object)))));
    });
  }

  /** Mostra una notifica di successo con il messaggio passato in input */
  private showSuccess(text: string) {
    this.msgs.push({severity: 'success', summary: 'Success Message', detail: text});
    setTimeout(() => {
      this.clearNotifications();
    }, 2000);
  }

  /** Mostra una notifica di informazione con il messaggio passato in input */
  private showInfo(text: string) {
    this.msgs.push({severity: 'info', summary: 'Info Message', detail: text});
    setTimeout(() => {
      this.clearNotifications();
    }, 2000);
  }

  /** Mostra una notifica di warning con il messaggio passato in input */
  private showWarn(text: string) {
    this.msgs.push({severity: 'warn', summary: 'Warn Message', detail: text});
    setTimeout(() => {
      this.clearNotifications();
    }, 2000);
  }

  /** Mostra una notifica di errore con il messaggio passato in input */
  private showError(text: string) {
    this.msgs.push({severity: 'error', summary: 'Error Message', detail: text});
    setTimeout(() => {
      this.clearNotifications();
    }, 2000);
  }

  /** Ripulisce le notifiche */
  private clearNotifications() {
    this.msgs = [];
  }

  /** Usato per intercettare azioni di chiusura del modale */
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  private objToStrMap(obj) {
    let strMap = new Map();
    for (let k of Object.keys(obj)) {
      strMap.set(k, obj[k]);
    }
    return strMap;
  }

  private jsonToStrMap(jsonStr) {
    return this.objToStrMap(JSON.parse(jsonStr));
  }

  private getCountryFromFileName(name: string): string {
    const fileName = name.split('.json')[0];
    const country = fileName.length === 5
      ? fileName.substr(3, 5)
      : fileName.length > 1
        ? fileName.substr(0, 2)
        : fileName;
    return country
      ? country.toLocaleLowerCase()
      : '';
  }
}
