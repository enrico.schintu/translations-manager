import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {GrowlModule} from 'primeng/primeng';
import {PrettyJsonModule} from 'angular2-prettyjson';

import {TranslationsManagerComponent} from './translations-manager.component';
import {TranslationsManagerService} from './translations-manager.service';
import {UploadManagerComponent} from './upload-manager/upload-manager.component';
import {NgxFlagIconCssModule} from "ngx-flag-icon-css";
import {GoogleTranslateModule} from "../google-translate/google-translate.module";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModalModule,
    GrowlModule,
    PrettyJsonModule,
    NgxFlagIconCssModule,
    GoogleTranslateModule
  ],
   declarations: [
	  TranslationsManagerComponent,
	  UploadManagerComponent,
   ],
   exports: [
	  TranslationsManagerComponent
   ],
   providers: [
	  TranslationsManagerService
   ]
})
export class TranslationsManagerModule { }
