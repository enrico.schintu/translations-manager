export class TranslationDto {
	languageCode: string;
	translationCode: string;
	value: string;
	user: string;
	deleteFlag: boolean;
}
