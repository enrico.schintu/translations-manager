import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-upload-manager',
	templateUrl: './upload-manager.component.html',
	styleUrls: ['./upload-manager.component.scss']
})

export class UploadManagerComponent implements OnInit {

	@Output() translationsUploaded: EventEmitter<any> = new EventEmitter();

	public invalidInput: boolean;
	public translations: Map<string, Array<any>>;
	public fileList: Array<string> = new Array<string>();

	constructor() {
	}

	ngOnInit() {
	}

	onFileChange(event): void {
		this.fileList = new Array<string>();
		const files: FileList = this.validateInput(event);
		if (files) {
			Array.from(files).forEach(file => {
				this.convertJsonToObject(file);
			});
		} else {
			this.translations = null;
		}
	}

	onSubmit(): void {
		this.translationsUploaded.emit(this.translations);
	}

	private validateInput(event): FileList {
		// Non sono arrivati dei file
		if (!event.target.files || !event.target.files.length) {
			this.invalidInput = true;
			return null;
		}
		this.invalidInput = false;
		this.translations = new Map();
		const files: FileList = event.target.files;
		// Verifico l'estensione dei file
		Array.from(files).forEach(file => {
			this.fileList.push(file.name);
			if (!file.name.endsWith('.json')) {
				this.invalidInput = true;
				return null;
			}
		});
		return files;
	}

	private convertJsonToObject(file: File): void {
		const fileReader = new FileReader();
		fileReader.onload = (e) => {
			let result = JSON.parse(JSON.stringify(fileReader.result).trim());

			let textJson = JSON.parse(JSON.stringify(fileReader.result, null, '\t').trim());
			let x = this.flatJson(textJson);
			this.translations.set(file.name, result);
		};
		fileReader.readAsText(file);
	}

	private flatJson(data: string) {
		let result = {};
		let x = 0;

		function recurse(cur, prop) {
			if (Object(cur) !== cur) {
				result[prop] = cur;
			} else if (Array.isArray(cur)) {
				for (let i = 0, l = cur.length; i < l; i++) {
					recurse(cur[i], prop + '[' + i + ']');
					x = l;
				}
				if (x === 0) { result[prop] = []; }
			} else {
				let isEmpty = true;
				for (let p of cur) {
					isEmpty = false;
					recurse(cur[p], prop ? prop + '.' + p : p);
				}
				if (isEmpty && prop) { result[prop] = {}; }
			}
		}
		recurse(data, '');
		return result;
	}
}



