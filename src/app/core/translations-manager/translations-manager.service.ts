import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslationDto } from './translation';

@Injectable()
export class TranslationsManagerService {

	private TRANSLATION_API = `/tsv/support/multilanguage/manage`;

	constructor(private httpClient: HttpClient) {
		this.retrieveAvailableLanguages();
	}

	public retrieveAvailableLanguages(): any {
		return this.httpClient.get<string[]>(`${this.TRANSLATION_API}`);
	}

	public retrieveLanguageTranslations(languageCode: string): any {
		return this.httpClient.get<TranslationDto[]>(`${this.TRANSLATION_API}/${languageCode}`);
	}

	public updateTranslations(translations: Array<TranslationDto>): any {
		return this.httpClient.post(`${this.TRANSLATION_API}`, translations);
	}
}
