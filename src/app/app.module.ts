import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {TranslationsManagerModule} from './core/translations-manager/translation-managers.module';
import {GoogleTranslateModule} from "./core/google-translate/google-translate.module";


@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		TranslationsManagerModule,
		HttpClientModule,
		NgbModule.forRoot(),
    GoogleTranslateModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
